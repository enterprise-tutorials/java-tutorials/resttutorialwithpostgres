package com.tutorial.postgres.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Business layer that interacts with underlying functions
 */
public class CustomerService {

	private static final Logger log = LoggerFactory.getLogger(CustomerService.class);

/*	@Autowired
	private CustomerRepository repository;

	public void insertRecord(long a_id, String a_name) {
		repository.save(new Greeting(a_id, a_name));
	}

	public boolean testCrud() {

		// save a couple of customers
		repository.save(new Greeting(1, "Mathew"));
		repository.save(new Greeting(2, "Skariah"));
		repository.save(new Greeting(3, "Nayanar"));
		repository.save(new Greeting(4, "Mamooty"));
		repository.save(new Greeting(5, "Mohanlal"));

		// fetch all customers
		log.info("Customers found with findAll():");
		log.info("-------------------------------");
		for (Greeting greet : repository.findAll()) {
			log.info(greet.toString());
		}
		log.info("");

		// fetch customers by last name
		log.info("Customer found with findByLastName('Bauer'):");
		log.info("--------------------------------------------");
		repository.findByLastName("Mathew").forEach(cust -> {
			log.info(cust.toString());
		});


		return true;
	}

	public Greeting findById(long a_id) {

		log.info("");
		repository.findById(a_id)
			.ifPresent(customer -> {
				log.info("Customer found with findById('" + a_id + "'");
				log.info("--------------------------------");
				log.info(customer.toString());
			});

		return null;
	}

	public Greeting findByLastName(String a_name) {

		log.info("");
		for (Greeting cust : repository.findByLastName(a_name)) {
			log.info("Customer found with findByLastName('" + a_name + "'");
			log.info("--------------------------------------------");
	        log.info(cust.toString());
	        return cust;
		}

		return null;
	}*/
}
