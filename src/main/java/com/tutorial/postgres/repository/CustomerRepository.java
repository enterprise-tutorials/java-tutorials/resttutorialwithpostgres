package com.tutorial.postgres.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tutorial.postgres.model.Greeting;

/**
 * Spring Data JPA focuses on using JPA to store data in a relational database.
 * Its most compelling feature is the ability to create repository
 * implementations automatically, at runtime, from a repository interface.
 */

@Repository
public interface CustomerRepository extends CrudRepository<Greeting, Long> {

    List<Greeting> findByLastName(String lastName);

    // Enabling ignoring case for an individual property
//    List<Greeting> findByLastNameIgnoreCase(String lastname);

    // Enabling static ORDER BY for a query
//    List<Greeting> findByLastnameOrderByLastNameAsc(String lastname);
}
