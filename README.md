## Summary
This is an example Java Spring Boot micro-service application showing functionality of REST API, Actuator, PostgresDB, Lombok, SwaggerUI, code coverage, code style and CI.

## Motivation
Encourage beginners and mid-level programmers to write a micro-service with all the minimal integrations. Having a database (like PostresDB) is optional for a micro-service.

## Build status
Build status of continuous integration with [travis](https://travis-ci.org/)
[![Build Status](https://travis-ci.org/lion01942/restTutorialWithPostgres.svg?branch=master)](https://travis-ci.org/lion01942/restTutorialWithPostgres)

## Prerequisites
* [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed
* [STS - Spring Tool Suite](https://spring.io/tools3/sts/all) installed if you have IDE

### Optional
* [GitHub Account](https://github.com/l) if you want to manage/maintain code
* [Travis-CI.org account](https://travis-ci.org/) if you want to trigger Continuous Integration (CI) job

## Installation Steps

### Clone the repo
```
$ git clone git@github.com:lion01942/restTutorialWithPostgres.git
```

### Build the application
```
$ ./gradlew clean build
```

### Run the tests
```
$ ./gradlew test
```

### Launch the application
```
$ java -jar build/libs/restTutorialWithPostgres-0.1.0.jar
```
Wait for the server to start....

### Validating the application

Open a new terminal and try the following commands:

**Check the health**

```
$ curl localhost:9001/actuator/health
```

If you see `{"status":"UP"}`, then you are good.

**Check the Swagger doc**
Open a new browser tab and go to:
```
http://localhost:9000/swagger-ui.html
```

If you are launching service with other micro-services and have routing rules, then use:
```
http://localhost:9000/restTutorialWithPostgres/api/swagger-ui.html
```

**Check the API**

```
$ curl localhost:9000/hello-world
```

## References:
* [Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
* [STS - Spring Tool Suite](https://spring.io/tools3/sts/all)
* [TravisCI](https://docs.travis-ci.com/user/languages/java/)
* [JaCoCo plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html)
* [Gradle](https://gradle.org/)
* [SwaggerUI](https://swagger.io/)


## Acknowledgement
I sencerely thank my mentors

## Author
* [Michael Skariah](https://www.linkedin.com/in/michael-skariah/)

## Blogs
* [coder-unleashed](https://medium.com/@coder_unleashed)
